
var gf = require('../gauss_functions.js');
var Matrix = require('../matrix.js');
var sinon = require("sinon");
const assert = require("assert");

describe("gauss_js", function() {
  it("makes me happy", function() {
    let m = gf.read_input();
    let mock = sinon.mock(m);
    
    mock.expects("exists_zero_row").once();
    mock.expects("exists_wrong_row").once();
    
    let golden_list = gf.read_golden();
    let output_list = gf.gauss(m);
    
    var r = gf.cmp_lists(output_list, golden_list);
    
    assert.strictEqual(r, true);
    mock.verify();
  });
});

